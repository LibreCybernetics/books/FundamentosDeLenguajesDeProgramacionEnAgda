all : pdf

pdf :
				pandoc -i metadata.yaml src/*.md -o 'FLPA.pdf' \
				--pdf-engine=lualatex -V links-as-notes=true
