# Prefacio

La conexión más profunda entre la lógica y la computación es un juego de palabras.
La doctrina de \textsf{Proposiciones-como-tipos} afirma que un cierto tipo de estructura formal puede leerse de dos formas:
o como proposiciones en lógica o como tipos en computación.
Además, una estructura relacionada se puede leer como la prueba de la proposición o como un programa del tipo correspondiente.
Más aún, la simplificación de pruebas corresponde con la evaluación de programas.

Por lo tanto, el titulo de este libro tiene dos lecturas.
Puede ser analizada como Fundamentos de Lenguajes de Programación en _Agda_ o Programando las Fundaciones de Lenguajes en _Agda_[^titulo] ---
las especificaciones que vamos a escribir en el asistente de pruebas de _Agda_ describen lenguajes de programación y son programas.

Este libro esta dirigido a estudiantes en el ultimo año de licenciatura o el primer año de maestría o doctorado.
Tiene como objetivo enseñar los fundamentos de las semánticas de operación de lenguajes de programación, con calculo lambda simplemente tipado ($\lambda^{\rightarrow}$) como el ejemplo principal.
Esta escrito como código literario en _Agda_.
La esperanza es que al usar un asistente de pruebas el desarrollo va a ser más concreto y accesible a los estudiantes y les dará retroalimentación inmediata para encontrar y corregir malentendidos.

El libro esta separado en dos partes. La primer parte, _Fundaciones Lógicas_, desarrolla los formalismos requeridos.
La segunda parte, _Fundamentos de Lenguajes de Programación_, introduce los métodos básicos de las semánticas operacionales.

## Comentarios Personales

Desde 2013 he enseñado un curso sobre Tipos y Semánticas para Lenguajes de Programación a estudiantes de 4^to^ año de licenciatura y de maestría en la Universidad de Edimburgo.
Una versión anterior de ese curso se basaba en el excelente libro de Benjamin Pierce: [_TAPL_](https://www.cis.upenn.edu/~bcpierce/tapl/).
Mi versión se basaba en el libro posterior de Pierce, [_Software Foundations_](https://softwarefoundations.cis.upenn.edu/), escrito en colaboración con otros y basado en _Coq_.
Estoy convencido de la reivindicación de Pierce de que basar un curso alrededor de un asistente de pruebas asiste el aprendizaje, como fue resumida en su ponencia principal en _ICFP_, [Lambda, The Ultimate TA](https://www.cis.upenn.edu/~bcpierce/papers/plcurriculum.pdf).

Sin embargo, después de 5 años de experiencia he llegado a la conclusión de que _Coq_ no es el mejor vehículo.
Demasiado del curso se enfoca en aprender tácticas para la derivación de pruebas a costa de aprender los fundamentos de la teoría de lenguajes de programación.
Cada concepto tiene que ser aprendido dos veces:
ej., tanto el tipo de dato producto como las tácticas correspondientes para introducir y eliminar conjunciones.
Las reglas que aplica _Coq_ para generar hipótesis de inducción pueden ser en ocasiones misteriosas.
Mientras el constructo `notation`{.coq} permite una sintaxis placenteramente flexible, puede ser confuso que un mismo concepto siempre tenga que tener dos nombres, ej. tanto `subst N x M`{.coq} y `N [x := M]`{.coq}.
El nombre de tacticas a veces es corto y a veces es largo;
las convenciones de nombres en la biblioteca estándar pueden ser salvajemente inconsistentes.
_\textsf{Proposiciones-como-tipos}_ como una fundación de demostración esta presente pero oculta.

Me encontre entusiasmado a replantear el curso en _Agda_.
En _Agda_ ya no se necesita aprender acerca de las tácticas:
solamente hay programación con tipos dependientes, sencillo y llano.
Introducción siempre es por un constructor, eliminación es siempre por coincidencia de patrones.
Introducción ya no es un concepto separado y misterioso pero corresponde a la noción familiar de recursion.
La sintaxis _Mixfix_ es flexible mientras se use solo un nombre por concepto,
ej., sustitución es `_[_:=_]`{.agda}.
La biblioteca estándar no es perfecta, pero hay un buen intento de consistencia.
Proposiciones-como-tipos como una fundación de demostración esta orgullosamente exhibida.

No hay un libro de teoría de lenguajes de programación en Agda. [_Verified Functional Programming in Agda_](https://www.morganclaypoolpublishers.com/catalog_Orig/samples/9781970001259_sample.pdf) de Stump cubre terreno relacionado, pero se enfoca más en programar con tipos dependientes más que en la teoría de lenguajes de programación.

La meta original era simplemente adaptar _Software Foundations_, manteniendo el mismo texto pero transpilar el código de _Coq_ a _Agda_.
Pero me fue prontamente aparente que tras 5 años en salones de clase tenía mis propias ideas acerca de como presentar el material.
Se dice que uno nunca debería escribir un libro a menos de que no pueda escribir el libro y pronto encontré que este era un libro que no podía escribir.

Soy afortunado que mi estudiante, Wen Kokke, estaba interesada en ayudar. Ella me guió como un novato de _Agda_ y me proporciono una infraestructura que es sencilla de usar y produce paginas que son un placer ver.

La mayor parte del texto fue escrito durante un sabático en la primer mitad de 2018.

\vspace{1em}

--- Philip Wadler, Río de Janeiro, Enero-Junio 2018

## Una palabra sobre los ejercicios

Ejercicios etiquetados _(recomendado)_ son los requeridos para los estudiantes en la clase enseñada en Edimburgo de este libro.

Ejercicios etiquetados _(extra)_ están para proveer un mayor reto. Pocos estudiantes realizan todos estos pero la mayoría intenta unos cuantos.

Ejercicios sin una etiqueta están incluidos para aquellas personas que quieran practica adicional.

[^titulo]: De su título en ingles “(Programming Language) Foundations in Agda” o “Programming (Language Foundations) in Agda”
