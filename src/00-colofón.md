\pagenumbering{gobble}

\clearpage

`\begin{adjustwidth}{2.5em}{2.5em}`{=latex}

\vspace*{\fill}

\centering

\small
**Traducción**
\footnotesize
Este libro es una traducción de _Programming Language Foundations in Agda_ de los mismos autores.

\vspace{2em}

La composición tipográfica se realizo con Pandoc y \LuaLaTeX.

\vspace{1em}

Los tipos usados son Montserrat, \textsf{Montserrat Alternates} y \texttt{Fira Code}. (Con glifos adicionales de \texttt{Fira Math})

\vspace{2em}

Este libro es público bajo la Licencia de Producción de Pares.

< <https://endefensadelsl.org/ppl_deed_es.html> >

\vspace{2em}

![](img/lpp.png){width=10em}\

\vspace{2em}

Fabián Heredia Montiel

Calor Mutuo

\vspace{1em}

\today

`\end{adjustwidth}`{=latex}

\clearpage
