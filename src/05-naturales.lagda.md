\ignore{
```agda
module 05-naturales where
```
}

# Los números naturales

> La boveda celeste tiene más estrellas de las que puedo contar,
> aunque menos de 5,000 son visibles a simple vista.
> El universo observable contiene cerca de 70×10^21^ estrellas.

> Pero el número de estrellas es finito, \
> mientras que los números naturales son infinitos. \
> Cuenta todoas las estrellas y te faltaran tantos números naturales como cuando empezaste.

## Los números naturales son un tipo de dato inductivo

Todas las personas concen los números naturales 0, 1, 2, 3, etc.
Escribimos $\mathbb{N}$ para denotar el _tipo_ de los números naturales,
indicados con la notación `0 : ℕ, 1 : ℕ, 2 : ℕ, 3 : ℕ`{.agda} , etc.

Este conjunto de números naturales es infinito, y aún así podemos escribir su definición en pocas lineas.
Aquí esta la definición como un par de reglas de inferencia:
\begin{equation}
\begin{aligned}[t]
\begin{array}{c}
\\
\hline
\texttt{cero} : \mathbb{N}
\end{array}
\end{aligned}
\hspace{2em}
\begin{aligned}[t]
\begin{array}{c}
\texttt{m} : \mathbb{N} \\
\hline
\texttt{suc m} : \mathbb{N}
\end{array}
\end{aligned}
\end{equation}

\needspace{4\baselineskip}
Y aquí esta la definición en _Agda_:
```agda
data ℕ : Set where
  cero : ℕ
  suc  : ℕ → ℕ
```

$\mathbb{N}$ es el nombre del tipo de dato que estamos definiendo y `cero`{.agda} y `suc`{.agda} (abreviación de sucesor) son los constructores del tipo de dato.

Ambas definiciónes nos dicen las mismas dos cosas:

 - Caso base: `cero` es un número natural.
 - Caso inductivo: si `m` es un número natural entonces `suc m` también es un número natural.

Más aún, estas dos reglas son la única forma de crear números naturales.
Entonces, los números naturales posibles son:

~~~{.agda}
cero
suc cero
suc (suc cero)
suc (suc (suc cero))
⋯
~~~

Escribimos 0 como abreviación para `cero`{.agda};
1 como abreviación para `suc cero`{.agda}, es decir, el natural que viene despúes de cero;
2 como abreviación para `suc (suc cero)`{.agda}, que es lo mismo que `suc 1`{.agda}, el sucesor de 1;
3 como abreviación para `suc (suc (suc cero))`{.agda};
etc.

#### Ejercicio `siete`

Expande la definición de 7.

### Examinando las reglas de inferencia

Examinemos las reglas de inferencia.
Cada regla de inferencia consiste de cero o más juicios escritos sobre una linea horizontal, llamados las hipótesis, y un solo juicio escrito debajo, llamado la conclusión.
La primer regla es el caso base;
no tiene hipótesis y la conclusión afirma que `cero`{.agda} es un natural.
La segunda regla es el caso inductivo;
tiene una hipótesis, que asume que `m`{.agda} es un número natural, y la conclusión afirma que `suc m`{.agda} también es un número natural.

### Examinando la definición de _Agda_

Examinemos la definición de _Agda_.
La palabra clave `data`{.agda} nos dice que esta es una definición inductiva, es decir que estamos definiendo un nuevo tipo de dato con constructores.
La fráse `ℕ : Set`{.agda} nos dice que $\mathbb{N}$ es el nombre del nuevo tipo de dato y que es un `Set`{.agda}, que es la forma de decir que es un tipo en _Agda_.
La palabra clave `where`{.agda} separa la declaración del tipo de dato de la declaración de sus constructores.
Cada constructor es declarado en su propia linea, que esta sangrada para indicar que pertenece a la correspondiente declaración `data`{.agda}.
Las lineas `cero : ℕ`{.agda} y `suc : ℕ → ℕ`{.agda} dan firmas especificando los tipos de los constructores `cero`{.agda} y `suc`{.agda}.
Nos dicen que `cero`{.agda} es un número natural y que `suc`{.agda} toma un número natural como argumento y regresa un número natural.

Puede que te hayas percatado que $\mathbb{N}$ y $\rightarrow$ no aparecen en tu teclado.
Son simbolos en _unicode_.
Al final de cada capítulo hay una lista de todos los simbolos _unicode_ presentados en el capitulo incluyendo instrucciónes de como escribirlos en el editor de texto _Emacs_.

## La historia de la creación

Miremos de nuevo las reglas que definen nuestros números naturales:

 - Caso base: `cero`{.agda} es un número natural.
 - Caso inductivo: si `m`{.agda} es un número natural, entonces `suc m`{.agda} también es un número natural.

¡Espera un minuto! La segunda linea define números naturales en terminos de números naturales.
¿Cómo es que eso esta permitido?
¿No es esta definición tan inutil como "_Brexit_ significa _Brexit_"?

De hecho, es posible asignarle a nuestra definición un significado sin usar circilaridades prohibidas.
Más aún, podemos hacerlo trabajando con conjuntos finitos y nunca hacer referencia al conjunto infinito de números naturales.

Pensaremos esto como una historia de la creación.
Para empezar, no sabemos de ningún número natural:

~~~{.agda}
-- Al principio no había ningún númmero natural
~~~

Ahora, podemos aplicar las reglas a todos los números naturales que conocemos.
El caso base nos dice que `cero`{.agda} es un número natural, entonces lo añadimos al conjunto de números naturales conocidos.
El caso inductivo nos dice que si `m`{.agda} es un número natural (en el día anterior a este) entonces `suc m`{.agda} es también un número natural (hoy). No conociamos ningún número natural antes de hoy, entonces el caso inductivo no aplica:

~~~{.agda}
--- En el primer día hay un número natural.
cero : ℕ
~~~

Entonces repetimos el proceso.
El siguiente día conocemos todos los números que conociamos el día anterior más cualquier número que hubieramos añadido por las reglas.
El caso base nos dice que `cero`{.agda} es un número natural, pero ya sabíamos eso.
Pero ahora el caso inductivo nos dice que ya que `cero`{.agda} era un número natural ayer, entonces `suc cero`{.agda} es un número natural hoy:

~~~{.agda}
-- En el segundo día hay dos números naturales.
cero : ℕ
suc cero : ℕ
~~~

Y volvemos a repetir el proceso.
Ahora el caso inductivo nos dice que ya que `cero`{.agda} y `suc cero`{.agda} son números naturales, entonces `suc cero`{.agda} y `suc (suc cero)` son números naturales.
Ya sabíamos del primero, pero el segundo es nuevo:

~~~{.agda}
-- En el tercer día hay tres números naturales.
cero : ℕ
suc cero : ℕ
suc (suc cero) : ℕ
~~~

Ya sabes los pasos para los siguientes días:

~~~{.agda}
-- En el cuarto día hay cuatro números naturales.
cero : ℕ
suc cero : ℕ
suc (suc cero) : ℕ
suc (suc (suc cero)) : ℕ
~~~

El proceso continua.
En el _n-esimo_ día va a haber _n_ diferentes naturales.
Cada número natural va a aparecer algún día.
En particular, el número _n_ aparece por primera vez en el _(n+1)-esmino_ día.
Y en realidad nunca definimos el conjunto de números en terminos de si mismo.
En vez, definimos el conjunto de números en el día _n+1_ en terminos del conjunto de números en el día _n_.

Un proceso como este se llama inductivo.
Empezamos con nada y construimos un conjunto potencialmente infinito aplicando reglas que convierten un conjunto finito a otro conjunto finito.

La regla que define cero se llama caso base porque introduce un número natural inclusive cuando no conocemos otros números naturales.
La regla que define sucesor se llama caso inductivo porque introduce más números naturales cuando ya conocemos algunos.
Observen el rol vital del caso base.
Si solo tuvieramos reglas inductivas entonces no tendríamos números de inicio, seguiriamos sin números el segundo día, y tampoco en el tercer día, etc. Una definición inductiva sin un caso base no sirve, como es la frase "_Brexit_ significa _Brexit_".

## Filosofía e historia

Un filósofo podría observar que nuestra referencia al primer día, segundo día, etc, involucra implicitamente entedender los números naturales.
En este sentido, nuestra definición podria ser considerada en algún sentido circular pero no necesitamos dejar que nos perturbe esto.
Todas las personas poseen un buen entendimiento informal de los números naturales que podemos tomar como fundacion para su descripción formal.

Mientras que los números naturales han sido entendidos desde que las personas pueden contar, la definición inductiva de los números naturales es relativamente reciente.
Se remonta al artículo _"Was sind und was sollen die Zahlen"_ (¿Qué son y qué deberían ser los números?) de Richar Dedekind publicado en 1888 y el libro _"Arithmetices principia, nova methodo exposita"_ (Los principios de la aritmética presentados por un nuevo método) de Giuseppe Peano publicado el siguiente año.

## Un pragma

En _Agda_, cualquier texto despúes de `--`{.agda} o encerrado entre `{-`{.agda} y `-}`{.agda} es considerado un comentario.
Los comentarios no tienen efecto en el código, con la excepción de un tipo especial de comentario, llamado un pragma, que esta encerrado entre `{-#`{.agda} y `#-}`{.agda}.

\needspace{3\baselineskip}
Incluir la linea
```agda
{-# BUILTIN NATURAL ℕ #-}
```
le dice a _Agda_ que `ℕ` corresponde a los números naturales y por lo tanto podremos escribir `0` como abreviación para `cero`{.agda}, `1` como abreviación para `suc cero`{.agda}, 2 como abreviación para `suc (suc cero)`{.agda}, etc.
La declaración no esta permitida a menos de qué el tipo dado tenga exatcamente dos constructores, uno sin argumentos (correspondiendo a `cero`) y el otro con un solo argumento del mismo tipo (correspondiendo a `suc`).

Aparte de permitir las abreviaciones el pragma también habilita una representación interna más eficiente de los naturales usando el tipo de _Haskell_ para enteros de tamaño arbitrario.
Representar a `n` con `cero` y `suc` require espacio proporcional a `n` mientras que representarlos como un entero de tamaño arbitrario en _Haskell_ solo require espacio proporcional al $\log n$.

## Importaciónes

En breve vamos a querer escribir unas ecuaciones que se cumplen entre terminos que involucran números naturales.
Para poder hacerlo necesitamos importar la definición de igualdad y notaciones para razonar sobre ella de la biblioteca estándar de _Agda_:

```agda
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _∎)
```

La primer linea trae el módulo de la biblioteca estándar que define la igualdad al alcance y le da el nombre `Eq`{.agda}.
La segunda linea abre ese modulo, es decir, añade todos los nombres especificados en la clausula `using`{.agda} al alcance actual.
En este caso los nombres añadidos son `_≡_`{.agda}, la relación de igualdad, y `refl`{.agda}, el nombre para la evidencia de que dos terminos son iguales.
La tercer linea toma un modulo que especifíca operadores para apoyar el razonamiento sobre equivalencia y añade todos los nombres especificados en la clausula `using`{.agda}
